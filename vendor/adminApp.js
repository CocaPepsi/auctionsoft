(function ($) {
    // console.log(window.location.href);
    logOut();
    var s = window.location.pathname;
    if(s === '/admin/'){
        var keyAdmin = localStorage.getItem("access_token_admin");
        if(keyAdmin){
            console.log(keyAdmin);
        }else{
            window.location.replace('login.html');
        }
    }
        var app = $.sammy('#app', function () {
          // this.use('Template');
  
          this.get('admin/#/', function (context) {
            context.app.swap('');
            context.render('templates/home.template')
              .appendTo(context.$element());
          });

          this.get('#/category', function (context) {
            this.partial('templates/category.template', function () {
                    getAllCategory();
                    $('#addNewCategory').on("click",function(){
                        addNewCategory();
                        
                    });
                    $('#list').on('click','#closeAddButton', function() {
                        var tr = $(this).closest('tr');
                        tr.remove();
                        $('.AddButton').css("display","block");
                    });
                    $('#list').on('click','#saveAddButton', function() {
                        var _catName = $('[name="txtCatName"]').val();
                        if (_catName.length === 0) {
                            alert('Please input a valid value');
                            return;
                        }
                    
                        var body = {
                            catName: _catName
                        };
                    
                        $.ajax({
                            url: 'http://localhost:3019/categories',
                            dataType: 'json',
                            timeout: 10000,
                    
                            type: 'POST',
                            contentType: 'application/json',
                            data: JSON.stringify(body)
                        }).done(function(data) {
                            swal("Good job!", "You clicked the button!", "success");
                            $('#list').empty();
                            $.ajax({
                                url: 'http://localhost:3019/categories',
                                dataType: 'json',
                                timeout: 10000
                            }).done(function(data) {
                                data.map((e,index)=>{
                                    var tr = '<tr>' +
                                        '<td>' +
                                        index +
                                        '</td>' +
                                        '<td>' +
                                        '<span id="content'+ e._id +'">' + e.catName + '</span>' +
                                        '<input id="show'+ e._id +'" class="hidden" name="txtCatName_'+ e._id +'" type="text" value="'+ e.catName +'">'+
                                        '</td>' +
                                        '<td>'+
                                            '<input data-id="'+ e._id + '" class="btn btn-success updButton" id="updButton'+ e._id +'" type="button" value="Update"> '+
                                            '<input data-id="'+ e._id +'" class="btn btn-danger delButton" id="delButton'+ e._id +'" type="button" value="Delete"> '+
                                            '<input data-id="'+ e._id + '" class="btn btn-success saveButton" id="saveButton'+ e._id +'" type="button" value="Save"> '+
                                            '<input data-id="'+ e._id + '" class="btn btn-danger closeButton" id="closeButton'+ e._id +'" type="button" value="Cancel"> '+
                                        '</td>' +
                                    '</tr>';
                                    $('#list').append(tr);
                                })
                            });
                        }).fail(function(xhr, textStatus, error) {
                            console.log(textStatus);
                            console.log(error);
                            console.log(xhr);
                        });
                        $('.AddButton').css("display","block");
                    });
                    $('#list').on('click','.updButton', function() {
                        var _id = $(this).data('id');
                        $('input#show'+_id).css("display","block");
                        $('#saveButton'+_id).css("display","-webkit-inline-box");
                        $('#closeButton'+_id).css("display","-webkit-inline-box");
                        $('#delButton'+_id).css("display","none");
                        $('span#content'+_id).css("display","none");
                        $('#updButton'+_id).css("display","none");
                    });

                    $('#list').on('click','.closeButton', function() {
                        var _id = $(this).data('id');
                        $('span#content'+_id).css("display","block");
                        $('#delButton'+_id).css("display","-webkit-inline-box");
                        $('#updButton'+_id).css("display","-webkit-inline-box");
                        $('#saveButton'+_id).css("display","none");
                        $('input#show'+_id).css("display","none");
                        $('#closeButton'+_id).css("display","none");
                    });

                    // ------------ Chức năng lưu thông tin update danh mục ------------
                    // 
                    // 
                    $('#list').on('click','.saveButton', function() {
                        var _id = $(this).data('id');
                        $('span#content'+_id).css("display","block");
                        $('#delButton'+_id).css("display","-webkit-inline-box");
                        $('#updButton'+_id).css("display","-webkit-inline-box");
                        $('#saveButton'+_id).css("display","none");
                        $('input#show'+_id).css("display","none");
                        $('#closeButton'+_id).css("display","none");
                        
                        // var _catName = $('#show'+_id).val();
                        var _catName = $('[name="txtCatName_'+_id+'"]').val();
                        var body = {
                            catName: _catName
                        };
                        console.log(_catName);
                        
                        document.getElementById("content"+_id).innerText=_catName;

                        $.ajax({
                            url: 'http://localhost:3019/categories/'+ _id,
                            dataType: 'json',
                            timeout: 10000,
                            type:"PUT",
                            contentType: 'application/json',
                            data: JSON.stringify(body)
                        }).done(function(data) {
                            swal("Good job!", "You clicked the button!", "success");
                            // $.sweetModal('This is an alert.');
                        }).fail(function(xhr, textStatus, error) {
                            console.log(error);
                            console.log(xhr);
                        });
                    });
                    // ------------------------------------------------------
                    // ------------ Chức năng xóa danh mục ------------
                    // 
                    // 
                    $('#list').on('click','.delButton', function() {
                        var tr = $(this).closest('tr');
                        var _id = $(this).data('id');
                        swal({
                            title: "Are you sure?",
                            text: "Once deleted, you will not be able to recover this imaginary file!",
                            icon: "warning",
                            buttons: true,
                            dangerMode: true,
                        })
                        .then((willDelete) => {
                            if (willDelete) {
                                $.ajax({
                                    url: 'http://localhost:3019/categories/'+ _id,
                                    dataType: 'json',
                                    timeout: 10000
                                }).done(function(data) {
                                    if(data.lstProduct.length > 0){
                                        swal("Không xóa được category đang có sản phẩm tồn tại!");
                                    }else{
                                        $.ajax({
                                            url: 'http://localhost:3019/categories/'+ _id,
                                            dataType: 'json',
                                            timeout: 10000,
                                            type:"DELETE"
                                        }).done(function(data) {
                                            tr.remove();
                                        }).fail(function(xhr, textStatus, error) {
                                            console.log(error);
                                            console.log(xhr);
                                        });
                                        swal("Poof! Your imaginary file has been deleted!", {
                                            icon: "success",
                                        });
                                    }
                                }).fail(function(xhr, textStatus, error) {
                                    console.log(error);
                                    console.log(xhr);
                                });
                            } else {
                                swal("Your imaginary file is safe!");
                            }
                        });
                    });
                    // 
                    // ------------ Kết thúc chức năng xóa danh mục
              });
          });
        //   ----------------------- User ------------------------------
        this.get('#/user', function (context) {
            this.partial('templates/user.template', function () {
                getAllUser();
                $(document).on("click",".sendVip",function(){
                    $("#list-users").html(" ");
                    var _id = $(this).data('id');
                    updateCatUserVip(_id);
                    $("#list-users").html(" ");
                    getAllUser();
                });
                $(document).on("click",".deleteUser",function(){
                    var _id = $(this).data('id');
                    var tr = $(this).closest('tr');
                    $.ajax({
                        url: 'http://localhost:3019/users/'+ _id,
                        dataType: 'json',
                        timeout: 10000,
                        type:"DELETE"
                    }).done(function(data) {
                        tr.remove();
                        swal("Good job!", "You clicked the button!", "success");
                        $("#list-users").html(" ");
                        getAllUser();
                    }).fail(function(xhr, textStatus, error) {
                        console.log(error);
                        console.log(xhr);
                    });
                    
                    
                });
              });
          });
        //   -------------------End User-----------------
        });
        $(function () {
            app.run('#/');
        });
  })(jQuery);

  
  var addNewCategory = () =>{
    var tr = '<tr>' +
        '<td>&nbsp;</td>' +
        '<td>' +
        '<input name="txtCatName" id="show" type="text" value="">'+
        '</td>' +
        '<td>'+
            '<input data-id="" class="btn btn-success" id="saveAddButton" type="button" value="Save">'+' '+
            '<input data-id="" class="btn btn-danger" id="closeAddButton" type="button" value="Close">'+
        '</td>' +
    '</tr>';
    $('#list').prepend(tr);
    $('.AddButton').css("display","none");
  }
  
  
var getAllCategory = () =>{
    $.ajax({
        url: 'http://localhost:3019/categories',
        dataType: 'json',
        timeout: 10000
    }).done(function(data) {
        var source = $('#list-category').html();
        var template = Handlebars.compile(source);
        html = template(data);
        $("#list").append(html);
    });
}

var getAllUser = () =>{
    $.ajax({
        url: 'http://localhost:3019/users/',
        dataType: 'json',
        timeout: 10000
    }).done(function(data) {
        var arrays = [];
        data.map((e,index)=>{
            if(e.dateGrant && e.dateExpired){
                e.dateGrant = e.dateGrant.split("T")[0];
                e.dateExpired = e.dateExpired.split("T")[0];
            }
            arrays.push(e);
        });
        // data.DateGrant = data.DateGrant.split('T');
        var source = $('#list-user').html();
        var template = Handlebars.compile(source);
        html = template(arrays);
        $("#list-users").append(html);
    });
}

var updateCatUserVip = (id) =>{
    $.ajax({
        url: 'http://localhost:3019/admin/'+ id+'/CheckReqOfUs',
        dataType: 'json',
        timeout: 10000
    }).done(function(data) {
        swal("Good job!", "You clicked the button!", "success");
    }).fail(function(xhr, textStatus, error) {
        console.log(error);
        console.log(xhr);
    });
}

var DeleteUser = (id) =>{
    
}

function logOut(){
    $(".logout").on("click",function(){
      localStorage.removeItem("access_token_admin");
      window.location.replace("/admin/login.html");
    });
  }