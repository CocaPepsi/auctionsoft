$('#btnLogin').on('click', function () {
    var poco = {
        eMailAdmin :  $('[name="eMailAdmin"]').val(),
        pass : $('[name="pass"]').val()
    };
    var jsonToPost = JSON.stringify(poco);
    $.ajax({
        url: 'http://localhost:3019/admin/login',
        dataType: 'json',
        timeout: 10000,
        type: 'POST',
        contentType: 'application/json',
        data: jsonToPost
    }).done(function(data) {
        console.log(data.access_token);
        localStorage.access_token_admin = data.access_token;
        window.location.replace('/admin');
    }).fail(function(xhr, textStatus, error) {
        console.log(textStatus);
        console.log(error);
        console.log(xhr);
        swal("Thông báo!", "Tài khoản hoặc mật khẩu chưa đúng!", "error");
    }); 
});