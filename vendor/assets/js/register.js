$('#btnRegister').on('click', function () {
    var poco = {
        eMailAdmin :  $('[name="eMailAdmin"]').val(),
        pass : $('[name="pass"]').val()
    };
    $.ajax({
        url: 'http://localhost:3019/admin',
        dataType: 'json',
        timeout: 10000,
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(poco)
    }).done(function(data) {
        window.location.replace('login.html');
    }).fail(function(xhr, textStatus, error) {
        console.log(textStatus);
        console.log(error);
        console.log(xhr);
    });  
});