(function ($) {
  HandlebarsIntl.registerWith(Handlebars);
  // Lấy tất cả danh mục đổ vào select option search
  getCategorySearch();
  getCategory();
  // -----------------------------------------------
  var user_logined = JSON.parse(localStorage.getItem('user'));
  var CUR_PAGE = 1;
  if (user_logined) {
    $("#login").hide();
    var showUser = '<li><a href="#/user/' + user_logined.payload.id + '" id="login">Xin chào: ' + user_logined.payload.email + '</a></li>'+
      '<li><a id="logOut">Log out</a></li>';
    $('.account-dropdown').append(showUser);
    logOut();
  }

  
  var s = window.location.href+'search/';
  // console.log(s);
  switch (s[1]) {
    case "admin":
      console.log("admin mà");
      break;
    default:
      var app = $.sammy('#app', function () {
        this.get('#/', function (context) {
          HandlebarsIntl.registerWith(Handlebars);
          this.partial('templates/home2.template', function () {
            GetProductTop5Price();
            LoadTop5_timeEnd();
            LoadTop5_countAuc();
            script();
            $(".zoomContainer").hide();
          });
          
        });

        this.get('#/about/:id', function (context) {
          var id = this.params['id'];
          this.partial('templates/about.html', function () {
            $(".zoomContainer").hide();
            $.ajax({
              url: 'http://localhost:3019/users/' + id,
              dataType: 'json',
              timeout: 10000
            }).done(function (data) {
              var compile = data.user.firstName +' '+ data.user.lastName;
              data.user.userNameFormat = Math.random().toString(36).slice(-5)+compile.slice(5);
              var source = $('#infor-detail').html();
              var template = Handlebars.compile(source);
              html = template(data.user);
              $(".information").append(html);
              var source2 = $('#formListComment').html();
              var template2 = Handlebars.compile(source2);
              html = template2(data.ass);
              $(".contentComment").append(html);
              $(document).on("click","#nhanxet",function(){
                  var user_logined = JSON.parse(localStorage.getItem('user'));
                  if(user_logined){
                    var body = {
                      userID : id,//ngừi bị đánh zá
                      IdAssess : user_logined.payload.id,//ngừi đáng zá
                      messenger : $('[name="message"]').val(),//lời nhắn
                      score : $('input[name=optradio]:checked', '#formNhanXet').val()
                    }
                    $.ajax({
                      url: 'http://localhost:3019/assessRoute/',
                      dataType: 'json',
                      timeout: 10000,
                      type: 'POST',
                      contentType: 'application/json',
                      data: JSON.stringify(body)
                    }).done(function (data) {
                      swal("Thành Công!", "Vui lòng nhấn OK để kết thúc!", "success");
                    }).fail(err=>{
                      console.log(err);   
                    });
                  } else{
                    swal("Thông báo!", "Vui lòng đăng nhập để được nhận xét!", "error");
                  }
                  
              });

              
            });
            // $.ajax({
            //   url: 'http://localhost:3019/users/'+id+'/lstAssess',
            //   dataType: 'json',
            //   timeout: 10000
            // }).done(function (dataNX) {
            //   console.log(dataNX.ass);
            //   var source = $('#formListComment').html();
            //   var template = Handlebars.compile(source);
            //   html = template(dataNX.ass);
            //   $(".information").append(html);
            //   dataNX.ass.map((e,i)=>{
            //     console.log(e.messenger);
            //     $(".appen").append(e.messenger);
            //   })
            // });
          });
        });

        this.get('#/user/:id', function (context) {
          HandlebarsIntl.registerWith(Handlebars);
          var id = this.params['id'];
          this.partial('templates/article-detail.html', function () {
            $.getScript("vendor/js/froala_editor.pkgd.min.js", function() {
              GetUserDetail(id);
              $.getScript("vendor/js/test.js", function() {});
            });

            // $("#addAuctionToUser").on("click",function(){
            //   alert($('[name="tudonggiahan"]').val());
            // });
            $("#btn_pricebuyNow").on("click",function(){
              var pricebuyNow = '<i class="fa fa-times" style="position: absolute;right: 0;top: 11px;" id="cancel" aria-hidden="true"></i><div class="form-group"><label for="giamuadut">Giá mua đứt</label><input type="number" name="pricebuyNow" class="form-control" id="giamuadut" placeholder="Ví dụ: 1200"></div>';
              $("._pricebuyNow").append(pricebuyNow);
              $("#__pricebuyNow").hide();
            });

            $(document).on("click","#cancel",function(){
              $("._pricebuyNow").html("");
              $("#__pricebuyNow").show();
            });
            
            Main_formAddAuctionFromUser(id);

            // Lấy tất cả sản phẩm khách hàng đã từng đăng
            $(".getAuctionProductOfUser").on("click", function () {
              GetAllAuctionProductOfUser(id);
            });

            // Lấy tất cả sản phẩm khách hàng đã từng đăng
            $(".getAllListLike").on("click", function () {
              getAllListLike(id);
            });

            // Lấy tất cả sản phẩm khách hàng đã từng đăng
            $(".getAllListAucing").on("click", function () {
              getAucProduct(id);
            });

            // Lấy tất cả sản phẩm khách hàng đã từng đăng
            $(".getAllListAucWin").on("click", function () {
              getAucProductWin(id);
            });

            // Lấy tất cả sản phẩm khách hàng đã từng đăng
            $(".getTimeProductOfUser").on("click", function () {
              GetAllTimeProductOfUser(id);
            });

            // Lấy tất cả sản phẩm khách hàng đã từng đăng
            $(".getAuctionEdProductOfUser").on("click", function () {
              GetAllAuctionEdProductOfUser(id);
            });

            
            
            
            // -----------------------------------
            $(document).on('click','#requestVip',function(){
                  swal({
                    title: "Nâng cấp VIP",
                    text: "Nâng cấp lên VIP sẽ tốn 700.000 VNĐ. Bạn có muốn tiếp tục?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                  })
                  .then((willDelete) => {
                    if (willDelete) {
                      UpdateVip(id);
                    } else {
                      swal("Your imaginary file is safe!");
                    }
                  });
            });
            $(document).on('click','#updateInfo',function(){
              var body = {
                pass: $('[name="password"]').val(),
                firstName :$('[name="firstname"]').val(),
                lastName :$('[name="lastname"]').val(),
                address : $('[name="address"]').val(),
                eMail: user_logined.payload.email
              };
              $.ajax({
                  url: 'http://localhost:3019/users/'+id,
                  dataType: 'json',
                  timeout: 10000,
                  type:"PUT",
                  contentType: 'application/json',
                  data: JSON.stringify(body)
              }).done(function(data) {
                    swal("Cập nhật thành công!", "Cập nhật xong rồi nha!", "success");
              }).fail(function(xhr, textStatus, error) {
                  console.log(error);
                  console.log(xhr);
              });
            });
          });
          
        });

        this.get('#/auction-detail/:id', function (context) {
          HandlebarsIntl.registerWith(Handlebars);
          $(".zoomContainer").hide();
          var id = this.params['id'];
          this.partial('templates/auction-detail2.html', function () {
            GetAuctionDetail(id);
            LoadTop5_countAuc();
            // Chọn sản phẩm yêu thích
            $(document).on("click","#sendAuction",function(){
              swal({
                title: "Thông báo",
                text: "Bạn có chắc muốn đấu giá với sản phẩm này?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
              })
              .then((willDelete) => {
                if (willDelete) {
                  updatePriceAuction(id);
                } else {
                  swal("Your imaginary file is safe!");
                }
              });
              
            });
            $(document).on("click","#kickUserAuction",function(){
              var idUs = $(this).data('id');
              kickUserAuction(id,idUs);
            });
            // Chọn sản phẩm yêu thích
            $(document).on("click","#like",function(){
              Like(id);
            });
            script();
          });
        });

        $(document).on("click","#btn-search-category",function(){
          var cat = $('[name="cat"]').val();
          var proName = $('[name="proName"]').val();
          localStorage.setItem("cat", cat);
          localStorage.setItem("proName", proName);
         
          var currentUrl = window.location.href.split("/#/");
          console.log(currentUrl);
          if(currentUrl[1] !== 'search/'){
            var formatURl = window.location.href.split("/#/");
            window.location.href= formatURl[0]+'#/search/';
          }
          else{
            $("#list-search").html('');
            resultSearch(1);
          }
        });

        this.get('#/search/', function (context) {
          HandlebarsIntl.registerWith(Handlebars);
          var id = this.params['id'];
          var CUR_PAGE = 1;    
          this.partial('templates/search.html', function () {
            $("#list-search").html('');
            resultSearch(CUR_PAGE);
            $(document).on("click","#loadMore",function(){
              CUR_PAGE++;
              resultSearch(CUR_PAGE);
            });
          });
        });

        this.get('#/updateDescribe/:id', function (context) {
          var id = this.params['id'];
          this.partial('templates/update-describe.template', function () {
            $.getScript( "vendor/js/froala_editor.pkgd.min.js", function() {
              updateDescribe(id);
            });
          });
        });
        
        this.get('#/:asd/:id', function (context) {
          HandlebarsIntl.registerWith(Handlebars);
          
          var id = this.params['id'];
          // this.item = this.items[this.params['id']];
          // if (!this.item) { return this.notFound(); }
          this.partial('templates/categoryProduct.template', function () {
            var CUR_PAGE = 1;                        
            $("#list-search").html('');
            getCategoryForPageCategoryProduct();
            getProduct(id,CUR_PAGE);
            $(document).on("click","#loadMore",function(){
              CUR_PAGE++;              
              getProduct(id,CUR_PAGE);
            });
          });
        });
        
        

        //   this.before('.*', function() {

        //       var hash = document.location.hash;
        //       $("nav").find("a").removeClass("current");
        //       $("nav").find("a[href='"+hash+"']").addClass("current");
        //  });

      });

      $(function () {
        app.run('#/');
      });
      break;
  }
})(jQuery);


var select = () =>{
  
}


//-----------------Lấy tất cả sản phẩm đấu giá mà khách hàng đã đăng------------------
var GetAllAuctionProductOfUser = (id) => { 
  var urlHost = window.location.href.substring(0, 22);
  $(".table-all-auction-product-to-user").html('');
  $.ajax({
    url: 'http://localhost:3019/users/'+id+'/ListProAvaiByUs',
    dataType: 'json',
    timeout: 10000
  }).done(function (data) {
    data.pro.map((e,index)=>{
      e.MainImg = 'http://localhost:3019/'+'imgs/'+e.strFile+'/'+e.lstImg[0];
    });
    // Kiểm tra giá mua đứt có tồn tại không
    data.pro.map((e,i)=>{
      if(e.timeEnd && e.postTime){
        e.timeEnd = e.timeEnd.split("T")[0];
        e.postTime = e.postTime.split("T")[0];
      }
      e.checkPricebuyNow = e.pricebuyNow ? e.checkPricebuyNow = true : e.checkPricebuyNow = false;
    });
    var source = $('#table-all-auction-product').html();
    var template = Handlebars.compile(source);
    html = template(data.pro);
    $(".table-all-auction-product-to-user").append(html);
  });
}
// ------------------------------------------------------------------------------------

//-----------------Lấy tất cả sản phẩm đấu giá mà khách hàng đã đăng------------------
var GetAllTimeProductOfUser = (id) => { 
  var urlHost = window.location.href.substring(0, 22);
  $(".table-all-time-product-to-user").html('');
  $.ajax({
    url: 'http://localhost:3019/users/'+id+'/ListProBeingAucByUs',
    dataType: 'json',
    timeout: 10000
  }).done(function (data) {
    data.pro.map((e,index)=>{
      e.MainImg = 'http://localhost:3019/'+'imgs/'+e.strFile+'/'+e.lstImg[0];
    });
    // Kiểm tra giá mua đứt có tồn tại không
    data.pro.map((e,i)=>{
      if(e.timeEnd && e.postTime){
        e.timeEnd = e.timeEnd.split("T")[0];
        e.postTime = e.postTime.split("T")[0];
      }
      e.checkPricebuyNow = e.pricebuyNow ? e.checkPricebuyNow = true : e.checkPricebuyNow = false;
    });
    var source = $('#table-all-time-product').html();
    var template = Handlebars.compile(source);
    html = template(data.pro);
    $(".table-all-time-product-to-user").append(html);
  });
}
// ------------------------------------------------------------------------------------

//-----------------Lấy tất cả sản phẩm đấu giá mà khách hàng đã đăng------------------
var GetAllAuctionEdProductOfUser = (id) => { 
  var urlHost = window.location.href.substring(0, 22);
  $(".table-all-AuctionEd-product-to-user").html('');
  $.ajax({
    url: 'http://localhost:3019/users/'+id+'/ListProDeposit',
    dataType: 'json',
    timeout: 10000
  }).done(function (data) {
    data.pro.map((e,index)=>{
      e.MainImg = 'http://localhost:3019/'+'imgs/'+e.strFile+'/'+e.lstImg[0];
    });
    // Kiểm tra giá mua đứt có tồn tại không
    data.pro.map((e,i)=>{
      if(e.timeEnd && e.postTime){
        e.timeEnd = e.timeEnd.split("T")[0];
        e.postTime = e.postTime.split("T")[0];
      }
      e.checkPricebuyNow = e.pricebuyNow ? e.checkPricebuyNow = true : e.checkPricebuyNow = false;
    });
    var source = $('#table-all-AuctionEd-product').html();
    var template = Handlebars.compile(source);
    html = template(data.pro);
    $(".table-all-AuctionEd-product-to-user").append(html);
  });
}
// ------------------------------------------------------------------------------------

//-----------------Lấy tất cả sản phẩm mà user thích------------------
var getAllListLike = (id) =>{
  $(".table-all-list-like-to-user").html('');
  $.ajax({
    url: 'http://localhost:3019/users/'+ id +'/viewWatchList',
    dataType: 'json',
    timeout: 10000
  }).done(function (data) {
    data.pro.map((e,index)=>{
      e.MainImg = 'http://localhost:3019/'+'imgs/'+e.strFile+'/'+e.lstImg[0];
    });
    // Kiểm tra giá mua đứt có tồn tại không
    data.pro.map((e,i)=>{
      e.timeEnd = e.timeEnd.split("T")[0];
      e.postTime = e.postTime.split("T")[0];
      e.checkPricebuyNow = e.pricebuyNow ? e.checkPricebuyNow = true : e.checkPricebuyNow = false;
    });
    var source = $('#table-all-list-like').html();
    var template = Handlebars.compile(source);
    html = template(data.pro);
    $(".table-all-list-like-to-user").append(html);
  }).fail(function(xhr, textStatus, error) {
    console.log(error);
    console.log(xhr);
  });
}
// ------------------------------------------------------------------------------------

//-----------------Lấy tất cả sản phẩm mà khách hàng đang đấu giá------------------
var getAucProduct = (id) =>{
  $(".table-all-list-aucing-to-user").html('');
  $.ajax({
    url: 'http://localhost:3019/users/'+ id +'/lstAucti',
    dataType: 'json',
    timeout: 10000
  }).done(function (data) {
    console.log(data);
    data.pro.map((e,index)=>{
      e.MainImg = 'http://localhost:3019/'+'imgs/'+e.strFile+'/'+e.lstImg[0];
    });
    // Kiểm tra giá mua đứt có tồn tại không
    data.pro.map((e,i)=>{
      if(e.timeEnd && e.postTime){
        e.timeEnd = e.timeEnd.split("T")[0];
        e.postTime = e.postTime.split("T")[0];
      }
      e.checkPricebuyNow = e.pricebuyNow ? e.checkPricebuyNow = true : e.checkPricebuyNow = false;
    });
    var source = $('#table-all-list-aucing').html();
    var template = Handlebars.compile(source);
    html = template(data.pro);
    $(".table-all-list-aucing-to-user").append(html);
  }).fail(function(xhr, textStatus, error) {
    console.log(error);
    console.log(xhr);
  });
}
// ------------------------------------------------------------------------------------

//-----------------Lấy tất cả sản phẩm mà khách hàng đang đấu giá------------------
var getAucProductWin = (id) =>{
  $(".table-all-list-aucing-win-to-user").html('');
  $.ajax({
    url: 'http://localhost:3019/users/'+ id +'/lstAucWin',
    dataType: 'json',
    timeout: 10000
  }).done(function (data) {
    console.log(data);
    data.pro.map((e,index)=>{
      e.MainImg = 'http://localhost:3019/'+'imgs/'+e.strFile+'/'+e.lstImg[0];
    });
    // Kiểm tra giá mua đứt có tồn tại không
    data.pro.map((e,i)=>{
      if(e.timeEnd && e.postTime){
        e.timeEnd = e.timeEnd.split("T")[0];
        e.postTime = e.postTime.split("T")[0];
      }
      e.checkPricebuyNow = e.pricebuyNow ? e.checkPricebuyNow = true : e.checkPricebuyNow = false;
    });
    var source = $('#table-all-list-aucing-win').html();
    var template = Handlebars.compile(source);
    html = template(data.pro);
    $(".table-all-list-aucing-win-to-user").append(html);
  }).fail(function(xhr, textStatus, error) {
    console.log(error);
    console.log(xhr);
  });
}
// ------------------------------------------------------------------------------------

//--------------- Lấy 8 sản phẩm có giá cáo nhất ---------------
var GetProductTop5Price = () => {
  var mang = [];
  $.ajax({
    url: 'http://localhost:3019/products/topPrice',
    dataType: 'json',
    timeout: 10000
  }).done(function (data) {
    // Kiểm tra giá mua đứt có tồn tại không
    data.map((e,i)=>{
      e.MainImg = 'http://localhost:3019/'+'imgs/'+e.strFile+'/'+e.lstImg[0];
      if(e.timeEnd){
        e.timeEnd = e.timeEnd.split("T")[0];
      }
      e.checkPricebuyNow = e.pricebuyNow ? e.checkPricebuyNow = true : e.checkPricebuyNow = false;
    });
    // element.compile = user.firstName +' '+ user.lastName;
    //         element.compile = Math.random().toString(36).slice(-5)+element.compile.slice(5);
      var source = $('#form-product-top-5-price').html();
      var template = Handlebars.compile(source);
      html = template(data);
      $(".product-carousel-top-5-price").append(html);
  });
}
//------------------------------------------------------------

//--------------- Lấy 8 sản phẩm có giá cáo nhất ---------------
var LoadTop5_countAuc = () => {
  $.ajax({
    url: 'http://localhost:3019/products/topAuc',
    dataType: 'json',
    timeout: 10000
  }).done(function (data) {
    data.map((e,index)=>{
      e.MainImg = 'http://localhost:3019/'+'imgs/'+e.strFile+'/'+e.lstImg[0];
    });
    // Kiểm tra giá mua đứt có tồn tại không
   data.map((e,i)=>{
     if(e.timeEnd){
      e.timeEnd = e.timeEnd.split("T")[0];
     }
    e.checkPricebuyNow = e.pricebuyNow ? e.checkPricebuyNow = true : e.checkPricebuyNow = false;
  });
    var source = $('#form-product-top-5-topAuc').html();
    var template = Handlebars.compile(source);
    html = template(data);
    $(".product-carousel-top-5-topAuc").append(html);
  });
}
//------------------------------------------------------------

//--------------- Lấy 8 sản phẩm có giá cáo nhất ---------------
var LoadTop5_timeEnd = () => {
  $.ajax({
    url: 'http://localhost:3019/products/topTimeEnd',
    dataType: 'json',
    timeout: 10000
  }).done(function (data) {
    data.map((e,index)=>{
      e.MainImg = 'http://localhost:3019/'+'imgs/'+e.strFile+'/'+e.lstImg[0];
    });
    // Kiểm tra giá mua đứt có tồn tại không
   data.map((e,i)=>{
    e.timeEnd = e.timeEnd.split("T")[0];
    e.checkPricebuyNow = e.pricebuyNow ? e.checkPricebuyNow = true : e.checkPricebuyNow = false;
  });
    var source = $('#form-product-top-5-countAuc').html();
    var template = Handlebars.compile(source);
    html = template(data);
    $(".product-carousel-top-5-countAuc").append(html);
  });
}
//------------------------------------------------------------

//--------------- Lấy thông tin của khách hàng ---------------
var froala = () => {
  $('#froala-editor').froalaEditor({
    toolbarButtons: ['fullscreen', 'bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', '|', 'fontFamily', 'fontSize', 'color', 'inlineStyle', 'paragraphStyle', '|', 'paragraphFormat', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote', '-', 'insertLink', 'insertImage', 'insertVideo', 'insertFile', 'insertTable', '|', 'emoticons', 'specialCharacters', 'insertHR', 'selectAll', 'clearFormatting', '|', 'print', 'help', 'html', '|', 'undo', 'redo']
  });
  $( "#datepicker" ).datepicker({ dateFormat: 'dd/mm/yy' });
  // $( "#datepickers" ).datepicker({dateFormat:"dd/mm/yy"}).datepicker("setDate",new Date());
}


var updateDescribe = (id) =>{
  $.ajax({
    url: 'http://localhost:3019/products/'+id,
    dataType: 'json',
    timeout: 10000
  }).done(function (data) {
    var source = $('#form-user-details').html();
    var template = Handlebars.compile(source);
    html = template(data);
    $("#zoo").append(html);
    data.describe.map((e,i)=>{
      document.getElementById("oldDescription").innerHTML = e.info_describe+'</br></br>';
    });
    
  });
}

var GetUserDetail = (id) => {
  $.ajax({
    url: 'http://localhost:3019/users/' + id,
    dataType: 'json',
    timeout: 10000
  }).done(function (data) {
    if(data.user.dateGrant && data.user.dateExpired){
      data.user.dateGrant = data.user.dateGrant.split("T")[0];
      data.user.dateExpired = data.user.dateExpired.split("T")[0];
    }
    var source = $('#form-user-detail').html();
    var template = Handlebars.compile(source);
    html = template(data.user);
    froala();
    $("#user-detail").append(html);
    data.user.catUser ? $("#postNewAuction").show() : $("#postNewAuction").hide();
    data.user.catUser ? $("#Vip").hide() : $("#Vip").show();
    // data.user.requirement ? $("#Vip").hide() : $("#Vip").show();
    data.user.requirement ? $("#Waitting").show() : $("#Waitting").hide();
  });

  $.ajax({
    url: 'http://localhost:3019/categories',
    dataType: 'json',
    timeout: 10000
  }).done(function (cate) {
    var str = '<select name="categories" id="selectbox-ex4" style="width: 200%" class="SlectBox" multiple="multiple">';
    cate.map((e,i)=>{ 
      str += "<option value='"+e._id+"'>"+e.catName+"</option>";
    });
    str += '</select>';
    $("#selectMulti").append(str);
  });
}

//------------------------------------------------------------

//--------------- Lấy thông tin chi tiết của từng sản phẩm ---------------
var GetAuctionDetail = (id) => {
  $.ajax({
    url: 'http://localhost:3019/products/' + id,
    dataType: 'json',
    timeout: 10000
  }).done(function (data) {
    console.log(data);
    data.checkAuction = data.timeEnd === null ? false : true;
    // Kiểm tra tài khoản đăng nhập có phải là chủ của sản phẩm không
    var CheckUserIsPoster = JSON.parse(localStorage.getItem('user'));
    if(CheckUserIsPoster){
      data.CheckUserIsPoster = data.idSeller === CheckUserIsPoster.payload.id ? true : false;
    }
    // Kiểm tra giá mua đứt có tồn tại không
    data.checkPricebuyNow = data.pricebuyNow ? data.checkPricebuyNow = true : data.checkPricebuyNow = false;
    // Thay đổi định dạng ngày kết thúc đấu giá
    data.timeEnd ? data.formatTimeEnd = data.timeEnd.split("T")[0]: null;
    // Thay đổi chuỗi url lấy hình của sản phẩm đấu giá
    data.MainImg = 'http://localhost:3019/'+'imgs/'+data.strFile+'/'+data.lstImg[0];
    var source = $('#form-auction-detail').html();
    var template = Handlebars.compile(source);
    html = template(data);
    $("#auction-details").append(html);
    $("#GiaHienTai").html("");
    $("#GiaHienTai").append(formatNumber(data.price)+" VNĐ");
    $(".currentPrice").append(formatNumber(data.price)+" VNĐ");
    // $(".numbers").each(function() {
    //   $(this).format({format:"#,###", locale:"us"});
    // });
    var user_logined = JSON.parse(localStorage.getItem('user'));
    // Kiểm tra khách hàng đó đăng nhập mới được đấu giá
    if(!user_logined){
      $("#Auction").hide();
      $(".userLike").hide();
      $(".add-to-buttons").append("<h3>Vui lòng đăng nhập để thích sản phẩm</h3>");
      $(".cart-qty-button").append("<h3>Vui lòng đăng nhập để đấu giá sản phẩm</h3>");
    }
    // Truyển số lượt đấu giá vào
    $("#countAuc").append(data.countAuc);
    // Truyền dữ liệu hình con của sản phẩm vào 
    data.lstImg.map((e,i)=>{
      var strGallery = '<li class="col-md-4">'+
      '<a class="elevatezoom-gallery" href="#" data-image="http://localhost:3019/imgs/'+data.strFile+'/'+e+'" data-zoom-image="http://localhost:3019/imgs/'+data.strFile+'/'+e+'">'+
      '<img src="http://localhost:3019/imgs/'+data.strFile+'/'+e+'" alt=""></a></li>';
      $("#gallery_01").append(strGallery);
    });
    // Truyền dữ liệu mô tả sản phẩm vào
    data.describe.map((e,i)=>{
      $("#reviews").append(e.info_describe);
      $("#reviews").append("</br>---------------</br>");
      $("#reviews").append("Ngày giờ: "+e.time_describe.split("T")[0]);
      $("#reviews").append("</br></br></br>");
    });
    // Lấy thông tin người đăng sản phẩm đấu giá
    
    $.ajax({
      url: 'http://localhost:3019/users/'+ data.idSeller,
      dataType: 'json',
      timeout: 10000
    }).done(function (dataUser) {
      $("#nameUserPost").append('<span>'+dataUser.user.firstName+' '+dataUser.user.lastName+'</span>');
      $(".diemdanhgiaNguoiDang").append('<span>'+dataUser.user.assess+'</span>');
      if(data.postTime && data.timeEnd){
        $('[name="timePost"]').val(data.postTime.split("T")[0]);
        $('[name="timeEnd"]').val(data.timeEnd.split("T")[0]);
      }
      $('[name="valuePriceAuction"]').val(0);
    });
    // lấy thông tin người đấu giá cao nhất
    if(data.idUserMaxPrice){
      $.ajax({
        url: 'http://localhost:3019/users/'+ data.idUserMaxPrice,
        dataType: 'json',
        timeout: 10000
      }).done(function (dataUserAuc) {
        var compile = dataUserAuc.user.firstName +' '+ dataUserAuc.user.lastName;
        compile = Math.random().toString(36).slice(-5)+compile.slice(5);
        
        $("#userPriceMax").attr("href", '#/about/'+dataUserAuc.user._id);
        $("#userPriceMax").append('<span>#'+compile+'</span>');
        $(".diemdanhgiaNguoiCaoNhat").append('<span>'+dataUserAuc.user.assess+'</span>');
      });
    }

    //Check đã Thích sản phẩm hay chưa
    $.ajax({
      url: 'http://localhost:3019/users/'+ user_logined.payload.id,
      dataType: 'json',
      timeout: 10000
    }).done(function (data_user_logined) {
      data_user_logined.user.watchList.map((e,i)=>{
        if(e === id){$(".add-to-buttons").hide()}
      });
    });
    
    // lấy tất cả danh mục liên quan đến sản phẩm đó 
    $.ajax({
      url: 'http://localhost:3019/categories',
      dataType: 'json',
      timeout: 10000
    }).done(function (cate) {
      cate.map((e,i)=>{ 
        data.lstCateID.map((pro,j)=>{
          if(e._id === pro){
            var str = '<li><a href="#/'+stripUnicode(e.catName)+'/'+e._id+'">'+e.catName+'</a></li>';
            $(".tag-list").append(str);
          }
        });
      });
    });

    // lấy danh sách thông tin khách hàng từng đấu giá
    var allPriceAuction;
    data.lstIdUserAuc.map((da,i)=>{  
      da.idUS === data.idUserMaxPrice ? allPriceAuction= da.price: null;
    });
    $.ajax({
      url: 'http://localhost:3019/users/',
      dataType: 'json',
      timeout: 10000
    }).done(function (dataAllUser) {
      // var max = Math.max.apply(Math,allPriceAuction);
      data.lstIdUserAuc.map((e,i)=>{
        dataAllUser.map((user,i)=>{
          if(e.idUS===user._id){
            if(allPriceAuction === e.price){
              var listUsers = '<tr>'+
              '<td><a href="#/about/'+user._id+'">'+user.firstName + '' + user.lastName+'</a></td>'+
              '<td>'+user.assess+'</td>'+
              '<td class="numbers">'+formatNumber(e.price)+' VNĐ</td>';
              if(data.lstIdUserKick.length!=0){
                  // data.lstIdUserKick.map((element,i)=>{
                  //   element ===
                  // });
                  
                  jQuery.inArray(user._id,data.lstIdUserKick) >= 0 ? listUsers+='<td><input type="button" class="btn btn-danger" value="Black List"/></td></tr>' : data.checkAuction === false ? null: listUsers+='<td><input type="button" data-id="'+e.idUS+'" id="kickUserAuction" class="btn btn-primary" value="delete"/></td></tr>';
                  // listUsers+='<td><input type="button" data-id="'+e.idUS+'" id="kickUserAuction" class="btn btn-primary" value="delete"/></td></tr>'
              }else{
                data.checkAuction === false ? null : listUsers+='<td><input type="button" data-id="'+e.idUS+'" id="kickUserAuction" class="btn btn-primary" value="delete"/></td></tr>';
              }
            } 
            else{
              var listUsers = '<tr>'+
              '<td><a href="#/about/'+user._id+'">'+user.firstName + '' + user.lastName+'</a></td>'+
              '<td>'+user.assess+'</td>'+
              '<td class="numbers">'+formatNumber(e.price)+' VNĐ</td>';
              if(data.lstIdUserKick.length!=0){
                jQuery.inArray(user._id,data.lstIdUserKick) >= 0 ? listUsers+='<td><input type="button" class="btn btn-danger" value="Black List"/></td></tr>' : data.checkAuction === false ? null : listUsers+='<td><input type="button" data-id="'+e.idUS+'" id="kickUserAuction" class="btn btn-primary" value="delete"/></td></tr>';
                // jQuery.inArray(user._id,data.lstIdUserKick) ? listUsers+='<td><input type="button" class="btn btn-danger" value="Black List"/></td></tr>': listUsers+='<td></td></tr>';
              }else{
                listUsers+='<td></td></tr>';
              }
                
            }
            $("#contentTableUserAuction").append(listUsers);
          }
        });
      });
    });
  });
  
}
// ---------------------------------------------------------------------------

// --------------- Tổng hợp chức năng thêm sản phẩm đấu giá từ khách hàng --------------
//--------------- Chức năng thêm sản phẩm đấu giá (Children)---------------
var addAuctionToUser = (id) => {
  var imagesArray = [];
  var filelist = document.getElementById("multiplefiles").files || [];
    $.each(filelist, function(i, myfile) {
      imagesArray.push(myfile.name);
  });
  var categories = [
    {
      id : $('[name="categories"]').val()
    },
  ];
  var getValueRadio = $('input[name=tudonggiahan]:checked', '#formAddAuctionToUser').val();
  var autoIncreaseTime = getValueRadio == "true" ? true : false;
  var body = {
    proName: $('[name="proNames"]').val(),
    price: $('[name="price"]').val(),
    pricebuyNow: $('[name="pricebuyNow"]').val(),
    describe: $('#froala-editor').val(),
    timeAuc : parseInt($('[name="endTime"]').val()),    
    lstCateID: $('[name="categories"]').val(),
    jump: $('[name="jump"]').val(),
    autoIncreaseTime: autoIncreaseTime,
    lstImg: imagesArray,
    idSeller: id,
    assessSeller: 1
  };

  $.ajax({
    url: 'http://localhost:3019/products/',
    dataType: 'json',
    timeout: 10000,
    type: 'POST',
    contentType: 'application/json',
    data: JSON.stringify(body)
  }).done(function (data) {
    swal("Thành Công!", "Vui lòng nhấn OK để kết thúc!", "success");
  }).fail(err=>{
    console.log(err);   
  });
}

//--------------- chức năng thêm đẩy hình ảnh lên server (Main)---------------
var Main_formAddAuctionFromUser = (id) =>{
    $('#formAddAuctionToUser').submit(function() {
      $(this).ajaxSubmit({
        success: function showResponse(responseText, statusText, xhr, $form) {
          addAuctionToUser(id);
        }
    });
    return false;
  });
}
//
// --------------- Tổng hợp chức năng thêm sản phẩm đấu giá từ khách hàng --------------

// -------------- Tổng hợp chức năng cập nhật thông tin cá nhân khách hàng --------------
var UpdateVip = (id) => {
  $.ajax({
    url: 'http://localhost:3019/users/'+id+'/Requirement',
    dataType: 'json',
    timeout: 10000
  }).done(function (data) {
    swal("Thành Công!", "Bạn đã gửi yêu cầu lên VIP. Vui lòng chờ hệ thống xử lý  ", "success");
    data.user.requirement ? $("#Vip").hide() : $("#Vip").show();
    data.user.requirement ? $("#Waitting").show() : $("#Waitting").hide();
  }).fail(err=>{
    swal("Wanning!", "You clicked the button!", "danger");
  });
}
// ------------------------------------------------------------------------------------

var resultSearch = (CUR_PAGE) =>{
  $.ajax({
    url: 'http://localhost:3019/products/search/detail?page='+CUR_PAGE+'&cat='+localStorage.getItem("cat")+'&proName='+localStorage.getItem("proName"),
    dataType: 'json',
    timeout: 10000
  }).done(function (data) {
    var dataFormat = [];
    data.lstPro.map((pro,i)=>{
      // Kiểm tra giá mua đứt có tồn tại không
      pro.checkPricebuyNow = pro.pricebuyNow ? pro.checkPricebuyNow = true : pro.checkPricebuyNow = false;
      pro.MainImg = 'http://localhost:3019/'+'imgs/'+pro.strFile+'/'+pro.lstImg[0];
      dataFormat.push(pro);
    });

    $.ajax({
      url: 'http://localhost:3019/users',
      dataType: 'json',
      timeout: 10000
    }).done(function (user) {
      user.map((us,i)=>{
        dataFormat.map((da,i)=>{
          if(us._id === da.idSeller){
            da.UserName = us.firstName + ' ' + us.lastName;
          }
        });
      });
      if(dataFormat.length >= 1){
        var source = $('#form-result-searchs').html();
        var template = Handlebars.compile(source);
        html = template(data.lstPro);
        $("#list-search").append(html);
      }else if(dataFormat){
        var source = $('#form-result-search').html();
        var template = Handlebars.compile(source);
        html = template(data.lstPro);
        $("#list-search").append(html);
      }
    });

    
    getCategoryForPageCategoryProduct();

    

    
  });
}

var loadAgainData = (id) =>{
  $.ajax({
    url: 'http://localhost:3019/products/' + id,
    dataType: 'json',
    timeout: 10000
  }).done(function (data) {
    $("#countAuc").html("");
    $("#countAuc").append(data.countAuc);
    $('[name="valuePriceAuction"]').val(0);
    // $('[name="valuePriceAuction"]').val(data.price);
    $("#GiaHienTai").html("");
    $("#GiaHienTai").append(formatNumber(data.price)+" VNĐ");
    
    // if(data.updatePriceAuction===0){
    //   $('[name="valuePriceAuction"]').val(data.price+data.jump);
    // }else{
    //   $('[name="valuePriceAuction"]').val(data.updatePriceAuction+data.jump);
    // }
    $.ajax({
      url: 'http://localhost:3019/users/',
      dataType: 'json',
      timeout: 10000
    }).done(function (dataAllUser) {
      $("#contentTableUserAuction").html("");
      data.lstIdUserAuc.map((e,i)=>{
        dataAllUser.map((user,i)=>{
          if(e.idUS===user._id){
            var listUsers = '<tr>'+
            '<td>'+user.firstName + '' + user.lastName+'</td>'+
            '<td>100</td>'+
            '<td class="numbers">'+formatNumber(e.price)+' VNĐ</td></tr>';
            $("#contentTableUserAuction").append(listUsers);
          }
        });
      });
    });
  });
}

var updatePriceAuction = (id) =>{
  var users_logined = JSON.parse(localStorage.getItem('user'));
  var priceAuction = $('[name="valuePriceAuction"]').val();
  $.ajax({
    url: 'http://localhost:3019/products/'+id,
    dataType: 'json',
    timeout: 10000
  }).done(function(data) {
      var allPriceMAX = [];
      data.top2.map((top,i)=>{
        allPriceMAX.push(top.PriceMax);
      });
      if(priceAuction==0 || (priceAuction >= 0 && priceAuction > Math.max.apply(Math,allPriceMAX))){
        var body = {
          idUS: users_logined.payload.id,
          PriceMax: $('[name="valuePriceAuction"]').val()
        };
        $.ajax({
            url: 'http://localhost:3019/products/'+id+'/AucPro',
            dataType: 'json',
            timeout: 10000,
            type:"PUT",
            contentType: 'application/json',
            data: JSON.stringify(body)
        }).done(function(data) {
            if(data.message === "you have been kich"){
              swal("Cảnh báo!", "Bạn đã bị kích khỏi đấu giá sản phẩm này!", "error");
            }else{
              swal("Đấu giá thành công!", "Vui lòng chờ kết quả đấu giá!", "success");
              loadAgainData(id);
            }
        }).fail(function(xhr, textStatus, error) {
            console.log(error);
            console.log(xhr);
        });
      }else{
        swal("Lỗi hệ thống!", "Số tiền đấu giá phải lớn hơn giá hiện tại!", "error");
      }
  }).fail(function(xhr, textStatus, error) {
      console.log(error);
      console.log(xhr);
  });
}

function formatNumber (num) {
  return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
}

var kickUserAuction = (id,idUs) =>{
  var body = {
    idUS: idUs
  };
  $.ajax({
    url: 'http://localhost:3019/products/'+id+'/Kich',
    dataType: 'json',
    timeout: 10000,
    type:"PUT",
    contentType: 'application/json',
    data: JSON.stringify(body)
  }).done(function(data) {
      swal("Kích thành công!", "Vui lòng nhấn ok để kết thúc!", "success");
      // loadAgainData(id);
  }).fail(function(xhr, textStatus, error) {
      console.log(error);
      console.log(xhr);
  });
}

var Like = (id) =>{
  var user_logined = JSON.parse(localStorage.getItem('user'));
  var body = {
    idPro : id
  };
  $.ajax({
      url: 'http://localhost:3019/users/'+user_logined.payload.id+'/addWatchList',
      dataType: 'json',
      timeout: 10000,
      type:"POST",
      contentType: 'application/json',
      data: JSON.stringify(body)
  }).done(function(data) {
      swal("Thành công!", "Vui lòng chờ kết quả đấu giá!", "success");
      //Check đã Thích sản phẩm hay chưa
      $.ajax({
        url: 'http://localhost:3019/users/'+ user_logined.payload.id,
        dataType: 'json',
        timeout: 10000
      }).done(function (data_user_logined) {
        data_user_logined.user.watchList.map((e,i)=>{
          if(e === id){$(".add-to-buttons").hide()}
        });
      });
  }).fail(function(xhr, textStatus, error) {
      console.log(error);
      console.log(xhr);
  });
}

var script = () =>{
    // $.getScript("vendor/js/bootstrap.min.js", function() {});
    $.getScript("vendor/lib/nivo-slider/js/jquery.nivo.slider.js", function() {});
    $.getScript("vendor/lib/nivo-slider/home.js", function() {});
    $.getScript("vendor/js/wow.min.js", function() {});
    $.getScript("vendor/js/jquery.meanmenu.js", function() {});
    $.getScript("vendor/js/owl.carousel.min.js", function() {});
    $.getScript("vendor/js/jquery-price-slider.js", function() {});
    $.getScript("vendor/js/jquery.scrollUp.min.js", function() {});
    $.getScript("vendor/js/jquery.countdown.min.js", function() {});
    $.getScript("vendor/js/jquery.sticky.js", function() {});
    $.getScript("vendor/js/jquery.elevateZoom-3.0.8.min.js", function() {});
    $.getScript("vendor/js/plugins.js", function() {});
    $.getScript("vendor/js/main.js", function() {});
    $.getScript("vendor/js/handlebars.js", function() {});
}


function getCategorySearch(){
  $.ajax({
    url: 'http://localhost:3019/categories',
    dataType: 'json',
    timeout: 10000
  }).done(function (cate) {
    cate.map((e,i)=>{ 
      var str = "<option value='"+e._id+"'>"+e.catName+"</option>";
      $("#searchCategory").append(str);
    });
    
  });
}

function getProduct(id,CUR_PAGE){
  $.ajax({
    url: 'http://localhost:3019/products?page=' + CUR_PAGE,
    dataType: 'json',
    timeout: 10000
  }).done(function (products) {
    var data = [];
    products.product.map((pro,i)=>{
      // Kiểm tra giá mua đứt có tồn tại không
      pro.checkPricebuyNow = pro.pricebuyNow ? pro.checkPricebuyNow = true : pro.checkPricebuyNow = false;
      pro.lstCateID.map((e,i)=>{
        if(e === id){ 
          pro.MainImg = 'http://localhost:3019/'+'imgs/'+pro.strFile+'/'+pro.lstImg[0];
          data.push(pro);
          
        }
      });
    });
    console.log(data);
    $.ajax({
      url: 'http://localhost:3019/users',
      dataType: 'json',
      timeout: 10000
    }).done(function (user) {
      user.map((us,i)=>{
        data.map((da,i)=>{
          if(us._id === da.idSeller){
            da.UserName = us.firstName + ' ' + us.lastName;
          }
        });
      });
      if(data.length >= 1){
        var source = $('#form-result-searchs').html();
        var template = Handlebars.compile(source);
        html = template(data);
        $("#list-search").append(html);
        if(products.check === false){
          $("#loadMore").hide();
        }
      }
    });
  });
}

function getCategory(){
  $.ajax({
    url: 'http://localhost:3019/categories',
    dataType: 'json',
    timeout: 10000
  }).done(function (cate) {
    cate.map((e,i)=>{ 
      var str = '<li class="current"><a href="#/'+stripUnicode(e.catName)+'/'+e._id+'">'+e.catName+'</a></li>';
      $("#nav").append(str);

      var str = '<li><a href="#/'+stripUnicode(e.catName)+'/'+e._id+'">'+e.catName+' ('+e.lstProduct.length+') </a></li>';
      $("#childrenLi").append(str);
    });
    
  });
}

function getCategoryForPageCategoryProduct(){
  $.ajax({
    url: 'http://localhost:3019/categories',
    dataType: 'json',
    timeout: 10000
  }).done(function (cate) {
    cate.map((e,i)=>{ 
      var str = '<li><a href="#/'+stripUnicode(e.catName)+'/'+e._id+'">'+e.catName+' ('+e.lstProduct.length+') </a></li>';
      $("#childrenLi").append(str);
    });
    
  });
}

function logOut(){
  $("#logOut").on("click",function(){
    localStorage.removeItem("user");
    localStorage.removeItem("access_token");
    window.location.replace("/login.html");
  });
}

function stripUnicode(slug) {
  slug = slug.toLowerCase(); 
  slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
  slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
  slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
  slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
  slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
  slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
  slug = slug.replace(/đ/gi, 'd');
  slug = slug.replace(/\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\:|\;|_/gi, '');
  slug = slug.replace(/ /gi, "-");
  slug = slug.replace(/\-\-\-\-\-/gi, '-');
  slug = slug.replace(/\-\-\-\-/gi, '-');
  slug = slug.replace(/\-\-\-/gi, '-');
  slug = slug.replace(/\-\-/gi, '-');
  slug = '@' + slug + '@';
  slug = slug.replace(/\@\-|\-\@|\@/gi, '');
  return slug;
}