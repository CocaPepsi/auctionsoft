$(document).ready(function(){
    // $('.datepicker').datepicker({
    //     autoclose: true,
    //     // format: 'dd/mm/yyyy',
    //     format: 'yyyy-mm-dd',
    //     startDate: '-3d'
        
    // });

    // $.validator.addMethod("vndate", function (value, element) {
    //     // return this.optional(element) || /^\d\d?\/\d\d?\/\d\d\d\d$/.test(value);
    //     return this.optional(element) || /^\d\d\d\d-\d\d?-\d\d?$/.test(value);
    // });

    // $('#registerForm').validate({
    //     rules: {
    //         UID: {
    //             required: true
    //         },
    //         PWD: {
    //             required: true,
    //             minlength: 6
    //         },
    //         ConfirmPWD: {
    //             required: true,
    //             equalTo: $('#txtPassword')
    //         },
    //         FullName: {
    //             required: true,
    //         },
    //         Email: {
    //             required: true,
    //             email: true
    //         },
    //         DOB: {
    //             required: true,
    //             vndate: true
    //         },
    //     },
    //     messages: {
    //         UID: {
    //             required: 'Please input UID'
    //         },
    //         PWD: {
    //             required: "Chưa nhập mật khẩu.",
    //             minlength: "Mật khẩu phải nhiều hơn 6 ký tự."
    //         },
    //         ConfirmPWD: {
    //             required: "Chưa nhập lại mật khẩu.",
    //             equalTo: "Mật khẩu nhập lại không khớp."
    //         },
    //         FullName: {
    //             required: "Chưa nhập họ tên.",
    //         },
    //         Email: {
    //             required: "Chưa nhập email.",
    //             email: "Email không đúng định dạng."
    //         },
    //         DOB: {
    //             required: "Chưa nhập ngày sinh.",
    //             vndate: 'FAILED'
    //         },
    //     },

    //     highlight: function (element) { // hightlight error inputs
    //         $(element)
    //             .closest('.form-group')
    //             .addClass('has-error'); // set error class to the control group
    //     },

    //     success: function (label) {
    //         // var name = label.attr('for');
    //         // $('[name=' + name + ']').closest('.form-group').removeClass('has-error');

    //         label.closest('.form-group').removeClass('has-error');
    //         label.closest('.form-group').addClass('has-success');
    //         label.remove();
    //     },

    //     errorElement: 'span',
    //     errorClass: 'help-block'
    // });

    // $('#txtUserName').select();

    // var isValid = $('#registerForm').valid();
});

$('#btnRegister').on('click', function () {
    if(grecaptcha.getResponse()!=""){
        var body = {
            captcha_response: grecaptcha.getResponse()
        };
        $.ajax({
            url: 'http://localhost:3019/users/captcha',
            dataType: 'json',
            timeout: 10000,
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(body)
        }).done(function(data) {
            if(data.success){
                var poco = {
                    firstName :  $('[name="firstName"]').val(),
                    lastName : $('[name="lastName"]').val(),
                    pass : $('[name="pass"]').val(),    
                    eMail :$('[name="eMail"]').val(),
                    address : $('[name="address"]').val(),
                    catUser : false
                };
                $.ajax({
                    url: 'http://localhost:3019/users',
                    dataType: 'json',
                    timeout: 10000,
                    type: 'POST',
                    contentType: 'application/json',
                    data: JSON.stringify(poco)
                }).done(function(data) {
                    if(data.message === "mail already exists."){
                        $('[name="firstName"]').val(" ");
                        $('[name="lastName"]').val(" ");
                        $('[name="pass"]').val("");
                        $('[name="eMail"]').val(" ");
                        $('[name="password_confirmation"]').val("");
                        $('[name="address"]').val(" ");
                        swal("Lỗi rồi!", "Email đã tồn tại vui lòng chọn email khác!", "error");
                        setInterval(function(){window.location.replace('register.html');},2000)
                    }else{
                        window.location.replace('index.html'); 
                    }
                }).fail(function(xhr, textStatus, error) {
                    console.log(textStatus);
                    console.log(error);
                    console.log(xhr);
                }); 
            }
        }).fail(function(xhr, textStatus, error) {
            console.log(textStatus);
            console.log(error);
            console.log(xhr);
        });
    }
    else{
        alert("captcha đâu");
    }
    
});