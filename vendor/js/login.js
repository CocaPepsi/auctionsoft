$(function () {
    $('#loginForm').validate({
        rules: {
            PWD: {
                required: true,
                minlength: 6
            },
            Email: {
                required: true,
                email: true
            }
        },
        messages: {
            PWD: {
                required: "Chưa nhập mật khẩu.",
                minlength: "Mật khẩu phải nhiều hơn 6 ký tự."
            },
            Email: {
                required: "Chưa nhập email.",
                email: "Email không đúng định dạng."
            }
        },

        highlight: function (element) { // hightlight error inputs
            $(element)
                .closest('.form-group')
                .addClass('has-error'); // set error class to the control group
        },

        success: function (label) {
            // var name = label.attr('for');
            // $('[name=' + name + ']').closest('.form-group').removeClass('has-error');

            label.closest('.form-group').removeClass('has-error');
            label.closest('.form-group').addClass('has-success');
            label.remove();
        },

        errorElement: 'span',
        errorClass: 'help-block'
    });


    $('#email').select();

    
});

$('#btnLogin').on('click', function () {
    var isValid = $('#loginForm').valid();
    if(isValid){
        var poco = {
            eMail :  $('[name="email"]').val(),
            pass : $('[name="password"]').val()
        };
        var jsonToPost = JSON.stringify(poco);
        $.ajax({
            url: 'http://localhost:3019/users/login',
            dataType: 'json',
            timeout: 10000,
            type: 'POST',
            contentType: 'application/json',
            data: jsonToPost
        }).done(function(data) {
            // alert(data);
            localStorage.access_token = data.access_token;
            console.log(localStorage.access_token);
           
        }).fail(function(xhr, textStatus, error) {
            console.log(textStatus);
            console.log(error);
            console.log(xhr);
            swal("Lỗi rồi!", "Tài khoản hoặc mật khẩu không đúng! Xin vui lòng nhập lại", "error");
        }); 
    }
});

$('#btnSecuri').on('click', function() {
    var isValid = $('#loginForm').valid();
        if(isValid){
        $.ajax({
            url: 'http://localhost:3019/secured',
            dataType: 'json',
            timeout: 10000,
            headers: {
                'x-access-token': localStorage.access_token
            }
        }).done(function(data) {
            localStorage.user = JSON.stringify(data);
            window.location.replace('/');
            // console.log(data);
        }).fail(function(xhr, textStatus, error) {
            console.log(textStatus);
            console.log(error);
            console.log(xhr);
        }); 
    }
});