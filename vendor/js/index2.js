(function($) {

    var app = $.sammy('#app', function() {
      // this.use('Template');
  
      // this.around(function(callback) {
      //   var context = this;
      //   this.load('data/articles.json')
      //       .then(function(items) {
      //         context.items = items;
      //       })
      //       .then(callback);
      // });
  
      this.get('#/', function (context) {
        context.app.swap('');
        $(".product-carousel-top-5-price").html('');
        context.render('banner.html').appendTo(context.$element());
        context.render('home.html', function () {
            GetProductTop5Price();
        }).appendTo(context.$element());
    });

    this.get('#/product/:id', function (context) {
        context.render('product-detail.html').appendTo(context.$element());
        // var str = window.location.href;
        // var strOne = str.split("#/");
        // var strs = '';
        // strOne.map((index,str)=>{
        //     strs = strs + index;
        // })
        // window.location.href = strs;
    });

    this.get('#/user/', function (context) {
        // $.ajax({
        //     url: 'http://localhost:4000/users/' + this.params['id'],
        //     dataType: 'json',
        //     timeout: 10000
        // })
        context.render('account.html').appendTo(context.$element());
    });
  
      // this.get('#/article/:id', function(context) {
      //   this.item = this.items[this.params['id']];
      //   if (!this.item) { return this.notFound(); }
      //   this.partial('templates/article-detail.template');
      // });
  
  
    //   this.before('.*', function() {
  
    //       var hash = document.location.hash;
    //       $("nav").find("a").removeClass("current");
    //       $("nav").find("a[href='"+hash+"']").addClass("current");
    //  });
  
    });
  
    $(function() {
      app.run('#/');
    });
  
  })(jQuery);

// Get product top 5 Price 
var GetProductTop5Price = () => {
    $.ajax({
        url: 'http://localhost:4000/products',
        dataType: 'json',
        timeout: 10000
    }).done(function (data) {
        var source = $('#form-product-top-5-price').html();
        var template = Handlebars.compile(source);
        html = template(data);
        $(".product-carousel-top-5-price").append(html);
    });
    
}